(function () {
    "use strict";
    var CC = {};
    CC.dom = {
        main: document.getElementById("main"),
        page: document.getElementsByClassName("page"),
        t1: document.getElementById("t1")
    };
    CC.step = 0;
    CC.page = -1;
    CC.each = function (array, callback) {
        var i;
        for (i = 0; i < array.length; i += 1) {
            callback(i, array[i]);
        }
    };
    CC.toggleClass = function (dom, classN) {
        if (dom.className.indexOf(classN) > -1) {
            dom.className = dom.className.replace(classN, "");
        } else {
            dom.className += " " + classN;
        }
    };
    CC.setTimer = function (time, callback) {
        setTimeout(callback, time * 1000);
    };
    CC.nextPage = function () {
        CC.page += 1;
        CC.toggleClass(CC.dom.page[CC.page], "active");
        if (CC.page > 0) {
            CC.toggleClass(CC.dom.page[CC.page - 1], "active");
        }
        CC.each(CC.dom.page[CC.page].getElementsByTagName("div"), function (index, element) {
            CC.toggleClass(element, "animated");
        });
        switch (CC.page) {
        case 2:
            CC.setTimer(6.2, function () {
                CC.nextPage();
            });
            break;
        case 3:
            CC.setTimer(2.2, function () {
                CC.nextPage();
            });
            break;
        case 5:
            CC.setTimer(7.2, function () {
                CC.nextPage();
            });
            break;
        }
    };
    CC.nextStep = function (x) {
        switch (x) {
        case 1:
            CC.toggleClass(CC.dom.main, "enter");
            CC.nextPage();
            break;
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
            CC.nextPage();
            break;
        }
    };
    CC.dom.main.onclick = function () {
        if (CC.step < 6) {
            CC.step += 1;
            CC.nextStep(CC.step);
            window.console.log("step: " + CC.step);
        }
    };
    CC.nextPage();
}());